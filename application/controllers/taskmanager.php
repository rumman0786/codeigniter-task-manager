<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Taskmanager extends CI_Controller{
	function __construct(){
		parent::__construct();
		$this->load->helper('form');
		$this->load->helper('html');
		$this->load->helper('url');
		$this->load->library('form_validation');
		$this->load->model('Taskmanager_model');
		// $this->check_isvalidated() ;
	}
	function index(){
		if($this->check_isvalidated()){
			$data = $this->Taskmanager_model->general();
			$data['query'] = $this->Taskmanager_model->getAllTasks();
			$this->load->view('showAllTask_view',$data);	
		}else{
			redirect('login');
		}
	}

	function addtask(){
		if($this->check_isvalidated()){
			$data = $this->Taskmanager_model->general();
			$this->load->view('taskAddForm_view',$data);	
		}else{
			redirect('login');
		}
		
	}

	function sendDataToModel(){
		if($this->input->post('submit_taskDetails')){
			$this->Taskmanager_model->addToDb();
		}
		redirect('/taskmanager/index/', 'refresh');
	}


	public function do_upload(){
		$this->check_isvalidated();
		$data = $this->Taskmanager_model->general();
		//initial configuration for setting rules of input fields
		$config = array(
			array(
			'field' => "ftaskName",
			'label' => 'Task Name',
			'rules' => 'required|xss_clean|trim|htmlspecialchars'
			),
			array(
			'field' => "ftaskDescription",
			'label' => 'Task Description',
			'rules' => 'required|xss_clean|trim|htmlspecialchars'
			)
		);
		
		
		$this->form_validation->set_rules($config);
		//initial configuration for setting rules of FILE input fields
		$files = $_FILES;
		// var_dump($files);
		$file_existance_flag = false ; // initially no file exists
        foreach ($files['userfile']['tmp_name'] as $index => $tmpName) {
        	$_FILES['userfile']['name']= $files['userfile']['name'][$index];
			if (empty($_FILES['userfile']['name']))
			{
				//if file doesnt exist then apply these rules
   		 		$this->form_validation->set_rules('userfile', 'Document'); //====== , 'required'
			}else{
				//this means file exist
				//which also means atleast one user exists, so set flag to only update the task name-description not files table
				$file_existance_flag = true ;
			}
		}
		
		//running the rules against inputs and checking them
		if ($this->form_validation->run() == FALSE)
		{
			$this->load->view('taskAddForm_view',$data);
			return;
		}
		
		$locationsArray = array();

		if($file_existance_flag === false){
			$this->Taskmanager_model->addToDb($locationsArray);	
		}else{
			foreach ($files['userfile']['tmp_name'] as $index => $tmpName) {
	        	$config = $this->set_upload_options();
	            $this->load->library('upload', $config);
	            $this->upload->initialize($config);

		        $_FILES['userfile']['name']= $files['userfile']['name'][$index];
		        $_FILES['userfile']['type']= $files['userfile']['type'][$index];
		        $_FILES['userfile']['tmp_name']= $files['userfile']['tmp_name'][$index];
		        $_FILES['userfile']['error']= $files['userfile']['error'][$index];
		        $_FILES['userfile']['size']= $files['userfile']['size'][$index]; 

				if (!empty($_FILES['userfile']['error'])) {
	                // some error occured with the file in index $index
	                // yield an error here
	                $msg[] = "Error:::::" . $_FILES['userfile']['error'];
	                echo "IN foreach :: IF :: file found</br>";
	                continue; // return false also immediately perhaps??
	            }
	            echo $path =  $config['upload_path'] . $_FILES['userfile']['name']; //path
	            array_push($locationsArray,  $config['upload_path'] . $_FILES['userfile']['name']);
	            echo "<br/>";
	            try
	            {
	            	$this->upload->do_upload();
	            }
	            catch(Exception $err)
	            {
	            	log_message("error",$err->getMessage());
	            	echo "eeeee";
	            	return show_error($err->getMessage());
	            }
	            $msg[] = "Data Added";
	            echo "IN foreach :: AFTER IF :: file moved";
	            echo "<br/>";
	        }
	        echo "<br/> after loop";
	        $this->Taskmanager_model->addToDb($locationsArray);	
		}
		echo "<br/> after model call";
		redirect('/taskmanager/index/', 'refresh');
	}


	function edit(){
		$this->check_isvalidated();
			$editId = $this->uri->segment(3);
			$query = $this->Taskmanager_model->get($editId);
			$data = $this->Taskmanager_model->general();
			$data['ftaskName'] 	= array('name' => 'ftaskName',
				'size' => 30,
				'value' => $query['taskName'] );
			$data['ftaskDescription'] 	= array('name' => 'ftaskDescription',
				'size' => 30,
				'value' => $query['taskDescription']
				);
			$data['ftaskId'] = $editId;
			$data['imageLocations'] = $this->Taskmanager_model->getAllLocations($editId);
			$this->load->view('edittask_view',$data);
	}





	/**
	*function called from edit form to update data,
	* i.e. taskname,task description,new image addition,delete of existing image
	*@ $updateId : id of the task to be edited
	*/
	function update(){
		$updateId = $this->uri->segment(3);
		// data['fileExists'] shows if data exists initially set to true,
		$data['fileExists'] = true ;
		//error message array
		$msg = array();
		//checks if file exists, if not then data['fileExists'] is set to false
		if (empty($_FILES['userfile']['tmp_name'])) {
            $msg[] = "No Files";
            $count = 0;
            $data['fileExists'] = false ;
            echo "no file found <br/>";
        } else {
        	//$count:: number of file chooser buttons not the actual number of file chosen
            $count = count($_FILES['userfile']['tmp_name']);
            echo "file found";
        }
        echo "COUNT:: " . $count . "<br>";//debugger

        $iterator = 0 ;
        $locationsArray = array();
        $files = $_FILES;
        foreach ($_FILES['userfile']['tmp_name'] as $index => $tmpName) {
        	$config = $this->set_upload_options();
            $this->load->library('upload', $config);
            $this->upload->initialize($config);

	        $_FILES['userfile']['name']= $files['userfile']['name'][$index];
	        $_FILES['userfile']['type']= $files['userfile']['type'][$index];
	        $_FILES['userfile']['tmp_name']= $files['userfile']['tmp_name'][$index];
	        $_FILES['userfile']['error']= $files['userfile']['error'][$index];
	        $_FILES['userfile']['size']= $files['userfile']['size'][$index]; 

			if (!empty($_FILES['userfile']['error'])) {
                // some error occured with the file in index $index
                // yield an error here
                $msg[] = "Error:::::" . $_FILES['userfile']['error'];
                echo "IN foreach :: IF :: file found</br>";
                continue; // return false also immediately perhaps??
            }
            echo $path =  $config['upload_path'] . $_FILES['userfile']['name']; //path
            array_push($locationsArray,  $config['upload_path'] . $_FILES['userfile']['name']);
            echo "<br/>";
            try
            {
            	$this->upload->do_upload();
            }
            catch(Exception $err)
            {
            	log_message("error",$err->getMessage());
            	echo "eeeee";
            	return show_error($err->getMessage());
            }
            $msg[] = "Data Added";
            echo "IN foreach :: AFTER IF :: file moved";
            echo "<br/>";
        }
        echo "<br/> after loop";
       	$this->Taskmanager_model->editEntry($updateId,$data,$locationsArray);
		echo "<br/> after model call";
		redirect('/taskmanager/index/', 'refresh');
	}

	function set_upload_options()
	{   
		//CI upload configuration array
		$config = array(
			'upload_path' => "./uploads/",
			'allowed_types' => "gif|jpg|png|jpeg|pdf",
			'overwrite' => FALSE,
			'max_size' => "20480000", // Can be set to particular file size , here it is 2 MB(2048 Kb)
			'max_height' => "768",
			'max_width' => "1024"
			);

		return $config;
	}


	function delete(){
		$deleteId = $this->uri->segment(3);
        $this->Taskmanager_model->deleteEntry($deleteId);
      	redirect('/taskmanager/index/', 'refresh');
	}


	function imageDelete(){
		// echo $taskId = $this->uri->segment(3);
		// echo "<br/>";
		// echo $imageName = $this->uri->segment(4);
		// echo "<br/>";
		$taskId =$_POST['taskId'];
		$imageName = $_POST['imageName'];
		$this->Taskmanager_model->deleteImage($taskId,$imageName);
		// redirect('/taskmanager/index/', 'refresh');
	}

	function logout(){
		$this->session->sess_destroy();
		redirect('login');
	}

	function check_isvalidated(){
		if(! $this->session->userdata('validated')){
			redirect('login');
		}else{
		return true ;	
		}
		
	}

	function yolo_function()
	{
		$this->load->view('yolo');
	}  
}
?>