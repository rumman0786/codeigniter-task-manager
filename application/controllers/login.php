<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Login extends CI_Controller{
	function __construct(){
		parent::__construct();
		$this->load->helper('form');
		$this->load->helper('url');
		$this->load->helper('html');
		$this->load->model('Login_model');
		
	}
	
	function loginControl(){
		if($this->input->post('submit_credentials')){
			$result = $this->Login_model->login_user();
			if(! $result){
				$msg = '<font color=red>Invalid user email and/or password.</font><br/>';
				$this->index($msg);
			}else{
				// echo '<br/>Congratulations, you are logged in.';
    			redirect('Taskmanager/index');	
			}			
		}
	}

	function index($msg = NULL){
		$data = $this->Login_model->general();
		$data['msg'] = $msg ;
		$this->load->view('login_view',$data);
	}
}
