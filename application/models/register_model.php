<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Register_model extends CI_Model{
	function __construct(){
		parent::__construct();
	}

	function general(){
		// $this->load->library('MyMenu');
		// $menu = new MyMenu;
		$data['base']       = $this->config->item('base_url');
		$data['css_url']        = $this->config->item('css_url');     
		$data['bootstrap']  = $this->config->item('bootstrap');     
		// $data['menu'] 		= $menu->show_menu();
		$data['webtitle']	= 'User List';
		$data['websubtitle']= 'List of all registered users';
		$data['webfooter']	= '© Task Manager| Register  Page | Developer -Rumman';				
		
		$data['forminput']  = 'Form Input';
		$data['username']	= 'Username';
		$data['email']		= 'Email';				
		$data['password']	= 'Password';
		
		$data['fusername'] 	= array('name' => 'username',
			'size' => 30 );
		$data['femail'] 	= array('name' => 'email',
			'size' => 30 );
		$data['fpassword'] 	= array('name' => 'password',
			'size' => 30 );
		return $data ;
	}

	function insert_user(){
		$this->load->database();
		$data = array(
			// 'userId'=>$this->input->post('fusername'),
			'username'=>$this->input->post('fusername'),
			'email'=>$this->input->post('femail'),
			'password'=>$this->input->post('fpassword')
			// 'salt'=>$this->input->post('fsalt'),
			);
		$this->db->insert('userlist',$data);
	}
}
?>