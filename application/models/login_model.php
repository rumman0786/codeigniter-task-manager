<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Login_model extends CI_Model{
	function __construct(){
		parent::__construct();
	}

	function general(){
		//=====================Strings defined from configuration==============================================================
		$data['base']       = $this->config->item('base_url');
		$data['bootstrap']  = $this->config->item('bootstrap');     
		$data['css_url']  = $this->config->item('css_url');     
		//=====================Strings========================================================================================
		$data['webtitle']	= 'Login Page';
		$data['webfooter']	= 'Task Manager| Login  Page | Developer -Rumman';				
		$data['loginInput'] = 'Login Input';
		$data['email']		= 'Email';				
		$data['password']	= 'Password';
		//=====================parameters in the input fields of forms also strings but params stored in arrays===============
		$data['loginEmail'] 	= array('name' => 'email',
			'size' => 30 );
		$data['loginPassword'] 	= array('name' => 'password',
			'size' => 30 );
		return $data ;
	}

	function login_user(){
		// $this->load->database(); //already added on autoload otherwise this line is required
		$userEmail = $this->security->xss_clean($this->input->post('email'));
		$userPassword = $this->security->xss_clean($this->input->post('password'));
		echo $userEmail." : ".$userPassword;
		$this->db->where('email',$userEmail);
		$this->db->where('password',$userPassword);
		$query = $this->db->get('userlist');

		if($query->num_rows == 1){
			$row = $query->row();
			$sessionData = array(
				'dbuserId' => $row->userId,
				'dbusername' => $row->username, 
				'dbemail' => $row->email,
				'validated' => true
				);
			$this->session->set_userdata($sessionData);
			return true;
		}
		return false;
	}
}
?>