<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Taskmanager_model extends CI_Model{
	function __construct(){
		parent::__construct();
		$this->load->library('session');
	}

	function general(){
		// $this->load->library('MyMenu');
		// $menu = new MyMenu;
		$data['base']       = $this->config->item('base_url');
		$data['css_url']        = $this->config->item('css_url');     
		$data['bootstrap']  = $this->config->item('bootstrap');     
		// $data['menu'] 		= $menu->show_menu();
		$data['webtitle']	= 'Task List';
		$data['websubtitle']= 'List of all tasks';
		$data['webfooter']	= 'Task Manager | Task Add Page | Developer -Rumman';				
		
		$data['forminput']  = 'Form Input';
		$data['taskName']	= 'task Name';
		$data['taskDescription']= 'task Description';				
		
		$data['ftaskName'] 	= array('name' => 'ftaskName',
			'size' => 30 );
		$data['ftaskDescription'] 	= array('name' => 'ftaskDescription',
			'size' => 30 );
		return $data ;
	}

	function addToDb($locationsArray){
		echo "in model 1";
		$this->load->database();
		$taskDetails = array(
			'taskName'=>$this->input->post('ftaskName'),
			'taskDescription'=>$this->input->post('ftaskDescription'),
			'userId' => $this->session->userdata('dbuserId')
			);
		$this->db->insert('tasklist',$taskDetails);

		$newTaskId = mysql_insert_id() ;
		if(!empty($locationsArray)){
				foreach ($locationsArray as $currentLocation) {
				$taskFileDetails = array(
					'taskId'=> $newTaskId,
					'location' => $currentLocation
					);
				$this->db->insert('task_file_map',$taskFileDetails);
			}	
		}
		
		return true;
	}

	function deleteEntry($id){
		$query = $this->db->get_where('task_file_map', array('taskId' => $id));
		foreach ($query->result() as $row)
		{
		 	echo $filelocation =  substr($row->location,0,10);	
		    echo "<br/>";
		    echo $fileName = substr($row->location,10);
		    echo "<br/>";
		    $old = getcwd(); // Save the current directory
		    echo chdir($filelocation);
		    echo unlink($fileName);
		    echo chdir($old); // Restore the old working directory  
		}
		$this->db->delete('tasklist', array('taskId' => $id)); 
		$this->db->delete('task_file_map', array('taskId' => $id)); 
	}


	function deleteImage($taskId,$imageName){
		$location = "./uploads/";// echo 
		echo $fullLocationWithImage = $location.$imageName ;
		$this->db->delete('task_file_map', array('taskId' => $taskId,'location' => $fullLocationWithImage)); 
		$old = getcwd(); // Save the current directory
        echo chdir($location);
        echo unlink($imageName);
        echo chdir($old); // Restore the old working directory  
		
	}

	function editEntry($editId,$bata,$locationsArray){
		$data = array(
			'taskName' => $this->input->post('ftaskName'),
			'taskDescription' => $this->input->post('ftaskDescription'),
			);
		$this->db->update('tasklist',$data,array('taskId'=>$editId));
		if($bata['fileExists']){
			echo "<br/>file exists";
			foreach ($locationsArray as $currentLocation) {
				echo $currentLocation."<br/>";
				$imageDB = array(
					'location' => $currentLocation,
					'taskId'=> $editId
					);
				$this->db->insert('task_file_map',$imageDB);
			}
		}
	}

	function get($id){
		$query = $this->db->get_where('tasklist',array('taskId'=>$id));
		return $query->row_array();		  
	}
	
	function getAllTasks(){
		$userId = $this->session->userdata('dbuserId');
		$query = $this->db->get_where('tasklist',array('userId'=>$userId));
		return $query->result();
	}

	function getAllLocations($taskID){
		$this->db->select('location');
		$query = $this->db->get_where('task_file_map',array('taskId'=>$taskID));
		return $query->result_array();
	}

}
?>