<?php

//sends pictures' location of specific task to showAllTask_view
if ( ! function_exists('get_data_array')){
	function get_data_array($taskId){
		$CI =& get_instance();
		$CI->db->select('location');
		$query = $CI->db->get_where('task_file_map',array('taskId'=>$taskId));
		return $query->result_array();
	}
}
?>