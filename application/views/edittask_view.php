<html>
<head>
	<link rel="stylesheet" type="text/css" 
	href="<?php echo "$base/$bootstrap"?>">
	<link rel="stylesheet" href="<?php echo $css_url."/showAll.css" ; ?>">
	<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
</head>
<div class="container">
<body>
	<div id="header">
		<?php $this->load->view('header'); ?>
	</div>
	<?php echo heading('Edit Page',3)?>

	<?php 
	$attributes = array('role' => 'form','class' => 'form-signin');
	$attributesFormFile = array('name' => 'userfile[]','type'=>'file','size'=>'20');//,'value'=>set_value('uploadedFile')
	 ?>
	<?php echo form_open_multipart('taskmanager/update/'.$ftaskId,'attributes'); ?>
	
	<div class="form-group">
	<?php echo "Task Name : ".form_input('ftaskName',$ftaskName['value']).br(); ?>
	</div><div class="form-group">
	<?php echo "Task Description : ".form_input('ftaskDescription',$ftaskDescription['value']).br(); ?>
	</div><div class="form-group">
	<?php
	foreach($imageLocations as $loc) { //mysql_fetch_array($result_loc)
		//strips the folder path of image and the file extension to keep only the file name
		$imageId = substr($loc['location'],10);//echo $imageId = substr($loc['location'], 10);
		$path = $base.$loc['location'] ;
		// echo "<br/>".$path = $base.$loc['location'] ;//$base. substr($loc['location'],10);
		?>
		<ul>
			<li>
				<img src ='<?php echo $path ?>' height = '100px' width = '100px'>
				<?php echo anchor('taskmanager/imageDelete/'.$ftaskId.'/'.$imageId,'Delete'); ?>
				<span class="glyphicon remove glyphicon-trash" onclick="imageDelete(<?php echo "'".$ftaskId."'"; ?>,<?php echo "'".$imageId."'"; ?>)"></span> 
			</li>
		</ul>
		
		<?php
	}
	?>
	</div>
	<div class="form-group">
	<?php echo form_upload($attributesFormFile); ?>
	<?php echo form_upload($attributesFormFile); ?>
	</div>
	<div class="form-group">
	<?php echo form_submit('edit','Edit!','class="btn btn-info"'); ?>
	</div>
	<?php


echo form_close();
?>


<script>
// $(document).ready(function(){
//   $("#b1").click(function(){
//     $("#div1").load('http://localhost/citaskmanager/index.php/taskmanager/yolo_function');

//   });
// });
//"http://localhost/citaskmanager/index.php/taskmanager/imageDelete/",
function imageDelete(taskId,imageName){
	var base_url = '<?php base_url() ?>';
	$.ajax({
		type: "POST",
		url: "http://localhost/citaskmanager/index.php/taskmanager/imageDelete/",
		data: {"taskId":taskId , "imageName":imageName},
		success: function(data){
			window.location.href= document.URL;
		},
		error: function(){
			alert("failure");
		}
	});
}
</script>


<div id="div1"><h2>Let jQuery AJAX Change This Text</h2></div>
<button id='b1'>Get External Content</button>


<div id="footer">
	<?php $this->load->view('footer'); ?>

</div>

</body>
</div>
</html>