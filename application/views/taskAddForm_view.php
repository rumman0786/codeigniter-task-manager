<html>
<head>
	<link rel="stylesheet" type="text/css" 
	href="<?php echo "$base/$bootstrap"?>">
	<!-- <link rel="stylesheet" href="<?php echo $css_url."/signin.css" ; ?>"> -->
	<link rel="stylesheet" href="<?php echo $css_url."/showAll.css" ; ?>">
	<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
</head>
<div class="container" id='bodyTag'>
	<body>
		<!-- Fixed navbar -->
		<nav class="navbar navbar-default navbar-fixed-top" role="navigation">
			<div class="container">
				<div class="navbar-header">
					<a class="navbar-brand" href="#">Task Manager</a>
				</div>
				<div id="navbar" class="navbar-collapse collapse">
					<ul class="nav navbar-nav">
						<li><?php echo "<a href='$base/index.php/taskmanager/index'>" ; ?>Tasks</a></li>
						<li class="active"><a href="#">New Task</a></li>
					</ul>
					<ul class="nav navbar-nav navbar-right">
						<li><?php echo "<a href='$base/index.php/taskmanager/logout'>" ; ?>Logout</a></li>
					</ul>
				</div><!--/.nav-collapse -->
			</div>
		</nav>
		<?php
		$attributesHeading = array('class' => 'form-signin-heading');
		echo heading("Add a task",2,$attributesHeading);
		$attributesForm = array('role' => 'form','class' => 'form-signin');
		$attributesFormTaskName = array('name' => 'ftaskName','size' => 44 ,'placeholder'=>'Task Name','value' => set_value('ftaskName') );
		$attributesFormTaskDescription = array('name' => 'ftaskDescription','placeholder'=>'Task Description','size' => 44 ,'value' => set_value('ftaskDescription') );
		$attributesFormFile = array('name' => 'userfile[]','type'=>'file','size'=>'20','value'=>set_value('userfile'));
		$attributesFormSubmit = array('class' => 'btn btn-lg btn-info btn-block', 'name'=>'submit_taskDetails');
		
		?>
		
		<?php echo form_open_multipart('taskmanager/do_upload',$attributesForm); ?>
		<div id="task-wraper">
			<div class="form-group">
				<?php echo form_error('ftaskName'); ?>
				<?php echo $taskName." : ".form_input($attributesFormTaskName).br(); ?>
			</div><div class="form-group">
				<?php echo form_error('ftaskDescription'); ?>
				<?php echo $taskDescription." : ".form_input($attributesFormTaskDescription).br(); ?>
			</div><div class="form-group">
				 <?php
				 if(form_error('userfile') != NULL){
				 echo form_error('userfile')."(All of them)<br/><br/>"; 	
				 }
				  ?>
				<?php echo form_upload($attributesFormFile); ?><span class="glyphicon remove glyphicon-trash" aria-hidden="true"></span>
			</div>
		</div>

		<div class="form-group">
			<button  type="button" id="add-more" class="btn btn-lg btn-info btn-block" >Add More Files</button>
		</div>
		
		<div class="form-group">
			<?php echo form_submit($attributesFormSubmit,'Add Task'); ?>
		</div>
		

		<?php echo form_close(); ?>
		
	<div class="footer">
      <div class="container">
        <p class="text-muted"><?php $this->load->view('footer'); ?></p>
      </div>
    </div>
	</body>
</div>

</div>
</html>

<script>
    $("#add-more").click(function(){
        var elements = <?php echo form_error('userfile'); ?>'<div class="form-group"><input type="file" name="userfile[]" id="add-more"/><span class="glyphicon remove glyphicon-trash" aria-hidden="true"></div>';
        $("#task-wraper").append(elements);
        // remove 
	$(".remove").click(function(){
            this.parentNode.parentNode.removeChild(this.parentNode);
        });        
    });

    $(".remove").click(function(){
            this.parentNode.parentNode.removeChild(this.parentNode);
        });
</script>