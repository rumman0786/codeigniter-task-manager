<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="icon" href="images/favicon.ico">
	<title>Login</title>
	<script src="js/respond.js"></script>
	<link rel="stylesheet" href="<?php echo "$base/$bootstrap" ; ?>">
	<link rel="stylesheet" href="<?php echo $css_url."/signin.css" ; ?>">
</head>
<div class="container">
	<body>
		<?php
		$attributesHeading = array('class' => 'form-signin-heading');
		echo heading("Please sign in",2,$attributesHeading);
		if(! is_null($msg)) echo $msg;
		$attributesForm = array('role' => 'form','class' => 'form-signin');
		$attributesFormSubmit = array('class' => 'btn btn-lg btn-info btn-block', 'name'=>'submit_credentials');
		$attributesFormEmail = array('class' => 'form-control','placeholder'=>'Email address','name' => 'email');
		$attributesFormPassword = array('class' => 'form-control','placeholder'=>'password' ,'name' => 'password');

		?>

		<?php echo form_open('login/loginControl',$attributesForm); ?>
		<div class="form-group">
			<!-- <input type="text" name="useremail" class="form-control" placeholder="Email address" required autofocus/><br /> -->
			<?php echo $email." : ".form_input($attributesFormEmail).br(); ?>
		</div><div class="form-group">
		<?php echo $password." : ".form_password($attributesFormPassword).br(); ?>
	</div><div class="form-group">
	<?php echo form_submit($attributesFormSubmit,'Login'); ?>
	<div>
		<p class="signup-helper">
			New to Task Manager?
			<a id="login-signup-link" href="<?php echo $base."/index.php/register/registration" ; ?>">Sign up now »</a>
		</p>
		
	</div>
</div>
<?php

echo form_close();
?>



<div id="footer">
	<?php $this->load->view('footer'); ?>

</div>
</body>
</div>

</div>
</html>

