<html>
<head>
	<link rel="stylesheet" type="text/css" 
	href="<?php echo "$base/$bootstrap"?>">
	<link rel="stylesheet" href="<?php echo $css_url."/showAll.css" ; ?>">
</head>
<div class="container">
	<body>
		<!-- Fixed navbar -->
		<nav class="navbar navbar-default navbar-fixed-top" role="navigation">
			<div class="container">
				<div class="navbar-header">
					<a class="navbar-brand" href="#">Task Manager</a>
				</div>
				<div id="navbar" class="navbar-collapse collapse">
					<ul class="nav navbar-nav">
						<li class="active"><a href="#">Home</a></li>
						<li><?php echo "<a href='$base/index.php/taskmanager/addtask'>" ; ?>New Task</a></li>
					</ul>
					<ul class="nav navbar-nav navbar-right">
						<li><?php echo "<a href='$base/index.php/taskmanager/logout'>" ; ?>Logout</a></li>
					</ul>
				</div><!--/.nav-collapse -->
			</div>
		</nav>

		<div id='table-padder'> 
			<table class="table table-responsive table-striped table-bordered table-hover" border="2">
				<tr>
					<th>Task Name</th>
					<th>Task Description</th>
					<th colspan="2">Action</th>
					<th>Content</th>
				</tr>
				<?php 
				$this->load->helper('dbquery');
				foreach ($query as $row) {?>
					<tr>
						<td><?php echo $row->taskName; ?> </td>
						<td><?php echo $row->taskDescription; ?> </td>
						<td><?php echo anchor('taskmanager/edit/'.$row->taskId,'Edit') ?> </td>
						<td><?php echo anchor('taskmanager/delete/'.$row->taskId,'Delete') ?></td>
						<?php   
						echo "<td>";
                        foreach (get_data_array($row->taskId) as $loc) {
                        	// var_dump($loc);
                            ?>
                            <img src = "<?php echo $base.$loc['location'] ?>" height = '100px' width = '100px'>
                            <?php
                        }
                        echo "</td>";
                        ?>
						
					</tr>

					<?php }?>
			</table>
		</div>
	</br>
	<div class="footer">
      <div class="container">
        <p class="text-muted"><?php $this->load->view('footer'); ?></p>
      </div>
    </div>

</body>
</div>
</html>