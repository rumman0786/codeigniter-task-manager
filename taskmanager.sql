-- phpMyAdmin SQL Dump
-- version 3.4.10.1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Nov 05, 2014 at 11:31 AM
-- Server version: 5.5.20
-- PHP Version: 5.3.10

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `taskmanager`
--

-- --------------------------------------------------------

--
-- Table structure for table `tasklist`
--

CREATE TABLE IF NOT EXISTS `tasklist` (
  `taskId` int(11) NOT NULL AUTO_INCREMENT,
  `taskName` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `taskDescription` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `userId` int(11) NOT NULL,
  PRIMARY KEY (`taskId`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=36 ;

--
-- Dumping data for table `tasklist`
--

INSERT INTO `tasklist` (`taskId`, `taskName`, `taskDescription`, `userId`) VALUES
(30, '4.48', '4.48', 15),
(31, '4.54', '4.54', 15),
(32, '5.14', '5.14', 15),
(33, '5.14', '5.14', 15),
(34, '5.17', '5.17', 15),
(35, '5.18', '5.18', 15);

-- --------------------------------------------------------

--
-- Table structure for table `task_file_map`
--

CREATE TABLE IF NOT EXISTS `task_file_map` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `taskId` int(11) NOT NULL,
  `location` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`,`taskId`),
  KEY `taskId` (`taskId`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=10 ;

--
-- Dumping data for table `task_file_map`
--

INSERT INTO `task_file_map` (`id`, `taskId`, `location`) VALUES
(4, 30, './uploads/'),
(5, 31, './uploads/'),
(6, 32, './uploads//Array'),
(7, 33, './uploads//Array'),
(8, 34, './uploads//2.jpg'),
(9, 35, './uploads//3.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `userlist`
--

CREATE TABLE IF NOT EXISTS `userlist` (
  `userId` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `salt` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`userId`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=16 ;

--
-- Dumping data for table `userlist`
--

INSERT INTO `userlist` (`userId`, `username`, `email`, `password`, `salt`) VALUES
(15, 'rumman', 'rumman@gmail.com', '123', '');

--
-- Constraints for dumped tables
--

--
-- Constraints for table `task_file_map`
--
ALTER TABLE `task_file_map`
  ADD CONSTRAINT `task_file_map_ibfk_1` FOREIGN KEY (`taskId`) REFERENCES `tasklist` (`taskId`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
